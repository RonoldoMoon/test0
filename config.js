const config = {
  db: {
    /* don't expose password or any sensitive info, done only for demo */
    //Oh well.. ;)
    
    host:     process.env.MYSQL_SERVER, //Sweet thursday.
    database: process.env.MYSQL_DB,
    
    user:     process.env.MYSQL_USER,
    password: process.env.MYSQL_PASS,
  },
  listPerPage: 10,  //Nested array ?? :D - why do the last items have commas??
};
module.exports = config;
