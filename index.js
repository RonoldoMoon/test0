const express = require("express");
const app     = express();

const port = 3000;

const TestRouter    = require("./routes/test-route");
const GetRouter     = require("./routes/get");
//const PutRouter     = require("./routes/put");
//const PostRouter    = require("./routes/post");
//const DeleteRouter  = require("./routes/delete");

app.use(express.json());

app.use(express.urlencoded({ extended: true, }));

app.get("/", (req, res) => { res.json({ message: "root ok" }); });

app.use("/test", TestRouter);
app.use("/get",  GetRouter);
//app.use("/put",  PutRouter);
//app.use("/post",  PostRouter);
//app.use("/delete",  DeleteRouter);

/* Error handler middleware */
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  console.error(err.message, err.stack);
  res.status(statusCode).json({ message: err.message });
  return;
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
