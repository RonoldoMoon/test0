const express     = require('express');
const ThisRouter  = express.Router();
const GetService  = require('../../services/get-company'); //does not exist yet


ThisRouter.get('/', async function(req, res, next) {

  res.header('Access-Control-Allow-Origin','*');//ZIM
  try {
    res.json(await GetService.Blam(req.query.pop));
    //res.json({ pop: req.query.balls });
  } catch (err) {
    console.error(`Error while in /routes/get-company.js `, err.message);
    next(err);
  }
});

module.exports = ThisRouter;
