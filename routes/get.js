const express          = require("express");
const ThisRouter       = express.Router();
const GetCompanyRouter = require("./company/get-company");

//const GetServiceJobsRouter = require("service-jobs/.js");
//const GetProjectJobsRouter = require("project-jobs/.js");
//const GetInventoryRouter   = require("inventory/.js");
//const GetConsumptionRouter = require("consumption/.js");
//const GetMetaRouter        = require("meta/.js");
/*
app.get("/", (req, res) => {
  res.json({ message: "GET ok" });
*/

//Grab this from the other file ;)

ThisRouter.get('/', async function(req, res, next) {

  res.header('Access-Control-Allow-Origin','*');  //Bypass CORS kinda;)
  
  try {
    res.json({ message: "get ok" });
    
  } catch (err) {
    console.error(`Error while in "get.js" :`, err.message);  //hopefuly not a string problem here.
    next(err);
  }
});

ThisRouter.use("/company", GetCompanyRouter);

module.exports = ThisRouter;
