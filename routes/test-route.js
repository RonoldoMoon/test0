const express     = require('express');
const ThisRouter  = express.Router();
const TestService = require('../services/test-service');


ThisRouter.get('/', async function(req, res, next) {

  res.header('Access-Control-Allow-Origin','*');//ZIM
  try {
    res.json(await TestService.getMultiple(req.query.page));
  } catch (err) {
    console.error(`Error while getting programming languages `, err.message);
    next(err);
  }
});

module.exports = ThisRouter;
